<html>
    <head>
        <title>CIS 322 REST-api demo</title>
    </head>

    <body>
        <h1>Calling listAll (implicit JSON)</h1>

        <?php
        $json = file_get_contents('http://laptop-service/listAll/');
        $obj = json_decode($json);
        $open = $obj->open;
        $close = $obj->close;

        echo "Open:<br>";
        foreach ($open as $o) {
            echo "$o<br>";
        }

        echo "Close:<br>";
        foreach ($close as $c) {
            echo "$c<br>";
        }
        ?>
 
        <h1>Calling listAll (CSV)</h1>
        <?php
        $csv = file_get_contents('http://laptop-service/listAll/csv');
        $obj = explode("\\n", $csv);

        foreach($obj as $o) {
            echo trim("$o<br>", '"');
        }
        ?>
        
        <h1>Calling listOpenOnly (implicit JSON)</h1>

        <?php
        $json = file_get_contents('http://laptop-service/listOpenOnly/');
        $obj = json_decode($json);
        $open = $obj->open;

        echo "Open:<br>";
        foreach ($open as $o) {
            echo "$o<br>";
        }

        ?>
 
        <h1>Calling listCloseOnly (CSV, top 1)</h1>
        <?php
        $csv = file_get_contents('http://laptop-service/listCloseOnly/csv?top=1');
        $obj = explode("\\n", $csv);

        foreach($obj as $o) {
            echo trim("$o<br>", '"');
        }
        ?>

    </body>
</html>
