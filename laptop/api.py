#Laptop Service

from flask import Flask, render_template, request, make_response
from flask_restful import Resource, Api
from pymongo import MongoClient
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
data = []

header = {'Content-Type': 'text/html'}

class ListAll(Resource):
    def get(self, rep="json"):
        rep = "json" if rep is None else rep
        items = db.tododb.find_one()
        

        if rep == "csv":
            ret = "open,close\n"
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret += data[2] + "," + data[3] + "\n"  # both open and close

        else:
            ret = {"open": [], "close": []}
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                ret["close"].append(data[3])

        
        return ret


class ListOpenOnly(Resource):
    def get(self, rep="json"):
        rep = "json" if rep is None else rep
        items = db.tododb.find_one()
        ret = []
        i = 0;

        amount = request.args.get('top')
        amount = 20 if amount is None else int(amount)

        if rep == "csv":
            ret = "open\n"
            for control in items['controls']:
                if i >= amount:
                    break
                ret += list(items['controls'][control].values())[2] + "\n"
                i+=1
        
        else:
            ret = {"open": []}
            for control in items['controls']:
                if i >= amount: break
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                i+=1

        return ret


class ListCloseOnly(Resource):
    def get(self, rep="json"):
        rep = "json" if rep is None else rep
        items = db.tododb.find_one()
        ret = []
        i = 0;

        amount = request.args.get('top')
        amount = 20 if amount is None else int(amount)
        
        if rep == "csv":
            ret = "close\n"
            for control in items['controls']:
                if i >= amount: break
                ret += list(items['controls'][control].values())[3] + "\n"
                i+=1
        
        else:
            ret = {"close": []}
            for control in items['controls']:
                if i >= amount: break
                data = list(items['controls'][control].values())
                ret["close"].append(data[3])
                i+=1

        return ret
        
# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll/', '/listAll/<rep>')
api.add_resource(ListOpenOnly, '/listOpenOnly/', '/listOpenOnly/<rep>')
api.add_resource(ListCloseOnly, '/listCloseOnly/', '/listCloseOnly/<rep>')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
